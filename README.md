# Agentifai Assistant Permissions

This plugin provides assistant permissions for android platform

## Installation

### Cordova
To install the plugin in your Cordova app, run the following:
```script
cordova plugin add https://bitbucket.org/agentifai/cordova-plugin-agentifai-assistant-permissions.git
```